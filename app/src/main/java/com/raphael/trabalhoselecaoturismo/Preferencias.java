package com.raphael.trabalhoselecaoturismo;

import android.content.Intent;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;

public class Preferencias extends PreferenceActivity {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            //carrega o Layout de preferências, o PreferenceScreen
            addPreferencesFromResource(R.xml.preferences);

            //cria um Intent de resultado e seta resultado OK
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        }
    }
