package com.raphael.trabalhoselecaoturismo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.txtValorTotal)
    TextView valorTotal;

    @BindView(R.id.listViewOpcoes)
    ListView listView;

    @BindArray(R.array.titulos)
    String[] titulos;

    @BindArray(R.array.descricoes)
    String[] descricoes;

    @BindArray(R.array.imagens)
    TypedArray imagens;

    @BindView(R.id.btnConfig)
    Button btnConfig;

    private View view;

    private FrameLayout imagem;

    private String nome = "";

    private Boolean almoco = false;

    private String formadepagamento = "";

    private String acompanhantes = "0";

    private SharedPreferences sharedPreferences;

    private CheckBox checkBoxes[];

    private List<String> check;

    private String isChecked[];

    private Double total;

    private Double passeio;

    private Double valorAlmoco;


    SharedPreferences.OnSharedPreferenceChangeListener sp = new
            SharedPreferences.OnSharedPreferenceChangeListener() {
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

                    setUsersPreferences();

                    Adapter adapter = new Adapter();

                    adapter.CalculaTotal();

                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        ButterKnife.bind(this);

        Adapter adapter = new Adapter();

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(adapter);

        sharedPreferences.registerOnSharedPreferenceChangeListener(sp);

        inicializa();

    }

    private void inicializa() {

        checkBoxes = new CheckBox[titulos.length];
        isChecked = new String[titulos.length];
        check = new ArrayList<>();

        total = 0.00;
        passeio = 0.00;

    }

    @OnClick(R.id.btnConfig)
    protected void ButtonConfig(View v) {

        Intent intent = new Intent(MainActivity.this, Preferencias.class);
        startActivityForResult(intent, 1);

    }

    public void setUsersPreferences() {

        nome = sharedPreferences.getString("nome", "");

        acompanhantes = sharedPreferences.getString("acompanhantes", "");

        almoco = sharedPreferences.getBoolean("almoco", false);

        formadepagamento = sharedPreferences.getString("list", "");

    }

    @OnClick(R.id.btnComprar)
    protected void ButtonComprar(View v) {

        Resumo();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray("CHECKBOXES", isChecked);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle saveInstanceState) {
        isChecked = saveInstanceState.getStringArray("CHECKBOXES");
    }

    private class Adapter extends BaseAdapter implements AdapterView.OnItemClickListener {

        @Override
        public int getCount() {
            return titulos.length;
        }

        @Override
        public Object getItem(int position) {
            return titulos[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater layoutInflater;

            if (isChecked[position] == null) {
                isChecked[position] = "false";
            }

            layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

            view = layoutInflater.inflate(R.layout.list_row, null);

            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            if (isChecked[position] == "true") {
                checkBox.toggle();
            }

            imagem = (FrameLayout) view.findViewById(R.id.imagens);
            imagem.setBackground(imagens.getDrawable(position));

            TextView titulo = (TextView) view.findViewById(R.id.titulos);
            titulo.setText(titulos[position]);

            TextView descricao = (TextView) view.findViewById(R.id.descricoes);
            descricao.setText(descricoes[position]);


            return view;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            checkBox.toggle();
            isChecked[position] = String.valueOf(checkBox.isChecked());

                if (isChecked[position] == "true") {
                    check.add(titulos[position]);
                }else{
                    check.remove(titulos[position]);
                }

            Valor(checkBox.isChecked());

        }

        private void Valor(Boolean checked) {

            if (checked) {
                passeio += 15.00;
            } else {
                passeio -= 15.00;
            }

            CalculaTotal();
        }

        public void CalculaTotal() {

            valorAlmoco = 0.00;

            Integer acompanhante;

            if (!acompanhantes.isEmpty()) {
                acompanhante = Integer.parseInt(acompanhantes);
            } else {
                acompanhante = 0;
            }

            if (almoco && passeio != 0) {
                valorAlmoco = 30.00 * (acompanhante + 1);
            }

            total = passeio * (acompanhante + 1) + valorAlmoco;

            valorTotal.setText(String.format("R$ %.2f", total));

        }

    }

    protected void Resumo() {

        String resumo = "";

        String pontosTuristicos = "";

        for (String ch : check) {
            pontosTuristicos += ch.concat(", ");
        }

        if (almoco) {

            resumo = "Olá, meu nome é " + nome + " e desejo conhecer o " + pontosTuristicos +"tenho (" + acompanhantes + ") acompanhante(s). " +
                    "O passeio deve incluir almoço. Estou ciente do total de R$" + valorTotal.getText().toString() + " para este passeio.";

        } else {

            resumo = "Olá, meu nome é " + nome + " e desejo conhecer o " + pontosTuristicos + "tenho (" + acompanhantes + ") acompanhante(s). " +
                    "O passeio não deve incluir almoço. Estou ciente do total de R$" + valorTotal.getText().toString() + " para este passeio.";

        }

        if (formadepagamento.equals("WhatsApp")) {
            enviarWhats(resumo);
        } else if (formadepagamento.equals("E-mail")) {
            enviarEmail(resumo);
        } else if (formadepagamento.equals("SMS")) {
            enviarSms(resumo);
        } else {
            Toast.makeText(this, "Nenhuma forma de pagamento selecionada", Toast.LENGTH_SHORT).show();
        }

    }

    protected void enviarEmail(String Message) {
        if (Message.length() <= 0) return;
        String addresses = "";

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:" + addresses));

        intent.putExtra(Intent.EXTRA_SUBJECT, "Turismo Curitiba - Confirmação");

        intent.putExtra(Intent.EXTRA_TEXT, Message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void enviarSms(String resumo) {
        if (resumo.length() <= 0) return;

        Intent intent = new Intent(Intent.ACTION_SENDTO);

        intent.setType("*/*");

        intent.setData(Uri.parse("smsto:"));

        intent.putExtra("sms_body", resumo);
        intent.putExtra(Intent.EXTRA_STREAM, "");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private void enviarWhats(String resumo) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, resumo);
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }

}



